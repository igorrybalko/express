var socket;

window.onunload = function(){
    socket.disconnect();
};
window.onload = function(){
    socket = io();
    var messages = [],
        messField = document.getElementById('mess_field'),
        chatArea = document.getElementById('chat_area'),
        loginform = document.getElementById('loginform'),
        chatform = document.getElementById('chatform');

    loginform.addEventListener('submit', function(e){
        e.preventDefault();
        username = document.getElementById('username').value.trim();
        if(username){
            socket.emit('login', {username: username});
            this.classList.add('hidden');
            chatform.classList.remove('hidden');
        }
    });

    chatform.addEventListener('submit', function(e){
        e.preventDefault();
        var text = messField.value;
        socket.emit('send', {message: text, username: username});
    });

    socket.on('message', function(data){
        if(data.message){
            messages.unshift(data.message);
            var html = '';
            for(var i = 0; i < messages.length; i++){
                html += messages[i];
            }
            chatArea.innerHTML = html;
        }else{
            console.log(data);
        }
    });

    socket.on('userList', function(data){
        document.getElementById('userlist').innerHTML = data.list;
    });
    
};