var util = require('util');

module.exports = {
    varDump: function(res, data){
                res.set({
              'Content-Type': 'text/html;charset=utf-8'
            });
            res.send('<pre>' + util.inspect(data) + '</pre>');
    },
    getDate: function(date){
            var createDate = new Date(Date.parse(date)),
                day = date.getDate(),
                mounth = + date.getMonth() + 1,
                year = date.getFullYear();

            if(day < 10){
                day = '0' + day;
            }
            if(mounth < 10){
                mounth = '0' + mounth;
            }
            return day + '.' + mounth + '.' + year;
    }
};