var socket = require('socket.io'),
    users = {};

function getUsers(obj){
    var tmp = [];
    for(var i in obj){
        tmp.push(obj[i])
    }

    return tmp.join(', ');
}

function getTime(){

    var date = new Date(),
        hours = date.getHours(),
        minutes = date.getMinutes();

    if(hours < 10)
        hours = '0' + hours;
    if(minutes < 10)
        minutes = '0' + minutes;

    return hours + ':' + minutes;
}

module.exports = function(listenApp){

    var io = socket.listen(listenApp);

    io.sockets.on('connection', function(client){

        if(Object.keys(users).length){
            var userList = getUsers(users);
            io.sockets.emit('userList', {list: userList});
        }

        client.on('login', function(data){
            client.emit('message', {message: '<div class="messwr">-- <span>Добро пожаловать,</span> <span>' + data.username + '</span> --</div>'});
            client.broadcast.emit('message', {message: '<div class="messwr">-- ' + data.username
            + ' присоединился к чату -- <div class="messtime">' + getTime() + '</div></div>'});

            users[client.id] = data.username;
            var userList = getUsers(users);
            io.sockets.emit('userList', {list: userList});

        });

        client.on('send', function(data){
            io.sockets.emit('message', {message:'<div class="messwr"><span class="uname">'
            + data.username + ':</span> <span class="txtmess">'
            + data.message + '</span><div class="messtime">' + getTime() + '</div></div>'});
        });

        client.on('disconnect', function(data){
            if(Object.keys(users).length > 1){
                client.broadcast.emit('message', {message: '<div class="messwr">-- ' + users[client.id]
                + ' покинул чат -- <div class="messtime">' + getTime() + '</div></div>'});
            }
            delete users[client.id];

            if(Object.keys(users).length){
                var userList = getUsers(users);
                io.sockets.emit('userList', {list: userList});
            }

        });

    });
};