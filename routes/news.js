var express = require('express'),
    router = express.Router(),
    topMenu = require('../models/topMenu')();

router.get('/', function(req, res, next) {
    res.render('pages/category', { 'title': 'Site', 'topMenu': topMenu });
});

router.get('/:item', function(req, res, next) {
    res.render('pages/item', { 'title': 'Site', 'topMenu': topMenu });
});

module.exports = router;