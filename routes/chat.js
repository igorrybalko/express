var express = require('express'),
    router = express.Router(),
    topMenu = require('../models/topMenu')();

/* GET chat page. */
router.get('/', function(req, res, next) {
    res.render('pages/chat', { 'title': 'Chat', 'topMenu': topMenu });
});

module.exports = router;