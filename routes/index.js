var express = require('express'),
    router = express.Router(),
    topMenu = require('../models/topMenu')();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('pages/index', { 'title': 'Site', 'topMenu': topMenu });
});

module.exports = router;