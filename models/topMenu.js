function topMenu(){
    //return [{"url":"home","name":"Home"},{"url":"chat","name":"Chat"},{"url":"about","name":"About"},{"url":"contacts","name":"Contacts"}];
    return {
      home: "Home",
      chat: "Chat",
      about: "About",
      contacts: "Contacts"
    };
}

module.exports = topMenu;